#!/usr/bin/env bb

;; https://exercism.org/tracks/clojure/exercises/cars-assemble

(ns cars-assemble)

(defn production-rate [speed]
  (let [success-rate (cond (= speed 0) 0.0
                           (<= speed 4) 1.0
                           (<= speed 8) 0.9
                           (<= speed 9) 0.8
                           (<= speed 10) 0.77
                           :else nil)]
    (* success-rate speed 221)))


(defn working-items [speed]
  (int (/ (production-rate speed) 60)))

(production-rate 1)
(working-items 6)
