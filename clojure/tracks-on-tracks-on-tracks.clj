#!/usr/bin/env bb

(ns tracks-on-tracks-on-tracks)

(defn new-list []
  "Creates an empty list of languages to practice."
  '())

(defn add-language [lang-list lang]
  "Adds a language to the list."
  (conj lang-list lang))

(defn first-language [lang-list]
  "Returns the first language on the list."
  (first lang-list))

(defn remove-language [lang-list]
  "Removes the first language added to the list."
  (rest lang-list))

(defn count-languages [lang-list]
  "Returns the total number of languages on the list."
  (count lang-list))

(defn learning-list []
  "Creates an empty list, adds Clojure and Lisp, removes Lisp, adds
  Java and JavaScript, then finally returns a count of the total number
  of languages."
  (let [result '()]
    (let [result (conj result "Clojure" "Lisp")]
      (let [result (remove #(= % "Lisp") result)]
        (let [result (conj result "Java" "JavaScript")]
          (count result))))))

(learning-list)
