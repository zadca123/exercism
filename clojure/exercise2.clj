#!/usr/bin/env bb

(ns lucians-luscious-lasagna)
(def expected-time 40)

(defn remaining-time [minutes]
  (- expected-time minutes))

(defn prep-time [layers]
  (* 2 layers))

(defn total-time [layers minutes]
  (+ (prep-time layers)
     minutes
     ;; (remaining-time minutes)
     ))

(total-time 1 30)
