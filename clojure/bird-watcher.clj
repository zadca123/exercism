#!/usr/bin/env bb

(ns bird-watcher)

(def last-week [0 2 5 3 7 8 4])

(defn today [birds]
  ;; (last birds)
  (get birds 6))

(defn inc-bird [birds]
  ;; (let [last-index (dec (count birds))
  ;;       today-birds-inc (inc (get birds last-index))]
  ;;   (assoc birds last-index today-birds-inc))
  (assoc birds
         (dec (count birds))
         (inc (today birds))))

(defn day-without-birds? [birds]
  (if (some #(= 0 %1) birds)
    true
    false))

(defn n-days-count [birds n]
  (reduce + (take n birds)))

(defn busy-days [birds]
  (reduce #(if (>= %2 5) (inc %1) %1) 0 birds))

(defn odd-week? [birds]
  (if (= birds [1 0 1 0 1 0 1])
      true
      false))
