#!/usr/bin/env -S emacs -Q --script
;;; -*- lexical-binding: t; -*-

(defun acronym (phrase)
  (let* ((result '()))
    (dolist (word (split-string phrase "[ \f\t\n\r\v-]"))
      (let ((first-letter (upcase (substring word 0 1))))
        (push first-letter result)))
    (apply #'concat (reverse result))))

(acronym "total satania death")
