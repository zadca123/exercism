#!/usr/bin/env -S emacs -Q --script
;;; -*- lexical-binding: t; -*-

(defun list-foldl (fun list accu)
  "Fold (reduce) each item in the list from left to right using the given function and initial accumulator."
  (if (null list)
      accu
    (let ((head (car list))
          (tail (cdr list)))
      (list-foldl fun tail (funcall fun accu head)))))

(defun list-foldr (fun list accu)
  (if (null list)
      accu
    (let ((head (car list))
          (tail (cdr list)))
      (funcall fun head (list-foldr fun tail accu)))))

(defun list-empty-p (list)
  (seq-empty-p list))

(defun list-sum (list)
  (if (null list)
      0
    (apply #'+ list)))

(defun list-length (list)
  (length list))

(defun list-append (list1 list2)
  (append list1 list2))

(defun list-reverse (list)
  (reverse list))

(defun list-concatenate (list1 list2 &rest LISTS)
  (apply #'append list1 list2 LISTS))

(defun list-filter (list predicate)
  (seq-filter predicate list))

(defun list-map (list fun)
  (mapcar (lambda (elem) (funcall fun elem)) list))

(provide 'list-ops)
;;; list-ops.el ends here
