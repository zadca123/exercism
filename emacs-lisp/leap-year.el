(defun leap-year-p (year)
  (if (and (= (mod year 4) 0)
           (or (= (mod year 400) 0)
            (not (= (mod year 100) 0))))
      t
    nil)
  )

(leap-year-p 1996)
(leap-year-p 2000)
(leap-year-p 1900)
