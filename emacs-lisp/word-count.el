#!/usr/bin/env -S emacs -Q --script
;;; -*- lexical-binding: t; -*-

(defun word-count (sentence)
  (let* ((count '())
         (sentence (replace-regexp-in-string "[:;!@#$%^&*(),.]" " " (downcase sentence))))
    (dolist (word (split-string sentence))
      (let* ((word (replace-regexp-in-string "^'+\\|'+$" "" word)) ;; trim '
             (cell (assoc word count))
             (key (car cell))
             (value (cdr cell)))
        (if cell
            (setcdr cell (+ value 1))
          (push (cons word 1) count))))
    count))

(word-count "can, can't, 'can't''")
