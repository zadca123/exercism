#!/usr/bin/env -S emacs -Q --script
;;; -*- lexical-binding: t; -*-


(defun sum-base (list-of-digits in-base)
  (let ((result '())
        (n (length list-of-digits)))
    (dolist (digit list-of-digits)
      (when (>= digit in-base)
        (error "Digit: %s cannot be bigger or equal than base: %s" digit in-base))
      (setq n (1- n))
      (push (* digit (expt in-base n)) result))
    (apply #'+ result)))


(defun rebase (list-of-digits in-base out-base)
  (let* ((sum-in-base (sum-base list-of-digits in-base))
         (result '()))
    (while (> sum-in-base 0)
      (push (mod sum-in-base out-base) result)
      (setq sum-in-base (/ sum-in-base out-base)))
    result))

(rebase '(0 1 1) 2 10)
