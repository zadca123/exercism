#!/usr/bin/env -S emacs -Q --script
;;; -*- lexical-binding: t; -*-

(defun armstrong-p (n)
  (let* ((str (number-to-string n))
         (len (length str))
         (sum 0))
    (dotimes (i len)
      (let* ((digit (string-to-number (substring str i (1+ i))))
             (term (expt digit len)))
        (setq sum (+ sum term))))
    (= sum n)))

(armstrong-p 153)
