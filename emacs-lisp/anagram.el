
(defun anagrams-for (subject candidates &optional idx original result)
  (let ((idx (or idx 0))
        (char-list (string-to-list subject))
        (char-list-len (length subject))
        (result (or result '())))

    (if (and (= idx (- char-list-len 1))
             (member subject candidates)
             (not (equal subject original)))
        (cons subject result)
      (print subject))


    (dotimes (j (- char-list-len idx))
      (let* ((j (+ j idx))
             (char-idx (nth idx char-list))
             (char-j (nth j char-list)))
        (setcar (nthcdr idx char-list) char-j)
        (setcar (nthcdr j char-list) char-idx)
        (print result)
        (anagrams-for (mapconcat #'char-to-string char-list "") candidates (+ idx 1) subject result)))
    )
  )

(anagrams-for "stone" '("stone" "tones" "banana" "tons" "notes" "Seton"))

(defun foo ()
  )
