;; https://exercism.org/tracks/emacs-lisp/exercises/roman-numerals

(defun to-roman (number)
  (let ((result "")
        (prev-char nil)
        (roman-numerals '((?M . 1000)
                          (?D . 500)
                          (?C . 100)
                          (?L . 50)
                          (?X . 10)
                          (?V . 5)
                          (?I . 1))))
    (while (> number 0)
      (dolist (pair roman-numerals)
        (let* ((char (car pair))
               (digit (cdr pair))
               (division (floor number digit)))
          (unless (= division 0)
            (setq result (concat result (char-to-string char)))
            (setq number (- number digit)))
          (when (and (length> result 3)
                     (string-suffix-p (make-string 4 char) result))
            (setq result (concat (substring result 0 (- (length result) 3)) (char-to-string prev-char))))
          (setq prev-char char))))
    result))
(to-roman 9)


(defun number-to-roman (number)
  "Convert a number to a Roman numeral."
  (let* ((numbers   '(1000  900  500  400  100   90   50   40   10    9    5    4    1))
         (numerals  '("M"  "CM"  "D"  "CD"  "C"  "XC"  "L"  "XL"  "X"  "IX"  "V"  "IV"  "I"))
         (result    '())
         (n number))
    (while numbers
      (let ((current (car numbers))
            (numeral (car numerals)))
        (while (>= n current)
          (push numeral result)
          (setq n (- n current)))
        (setq numbers (cdr numbers))
        (setq numerals (cdr numerals))))
    (apply #'concat (reverse result))))

;; Test the function
(message "%s" (number-to-roman 15)) ; Output: "MMXXIII"
